# This is a CadQuery script template
# Add your script code below
import cadquery as cq
from Helpers import show


# sample size: 2.5 x 2.5 x 0.025
# considered size for layer sample: 1 x 1 x 0.025

length = 12000
width = 12000
length = 5000
width = 8000
thickness = 250
wall_width = 200

layer_height = 0.866203
radius_rel = 0.38
defect_rel = 0.97
num_cells = 4

n_layers = num_cells*2 +1
layer_num = 0
num_rods= n_layers-abs(layer_num)

lattice_const = thickness/layer_height
radius = radius_rel*lattice_const
outer_point = lattice_const*num_cells*0.5 + (num_cells - abs(layer_num))*0.5*lattice_const

cut_section_xz= 2*radius+ num_rods*lattice_const +50

listOfRods = [(outer_point - j*lattice_const,0) for j in range(0, num_rods ) ]

res = cq.Workplane("XY").box(width, length, thickness) \
    .faces(">Z").workplane().rect(cut_section_xz,length-2*wall_width).cutThruAll()
# ---WORKS
bodyWithCylinders= res.faces("<Y").workplane() \
   .pushPoints(listOfRods) \
        .circle(radius).extrude(-length)





    # TESTING -----------
# .makeCylinder(radius, length, dir=Vector(0,1,0))

# .hole(thickness)
#     .faces(">Z").workplane().hole(center_hole_dia)    \
#     .faces(">Z").workplane() \
  #  .rect(length - 8.0, height - 8.0, forConstruction=True) \
#    .vertices().cboreHole(cbore_hole_diameter, cbore_diameter, cbore_depth)


# The show function will still work, but is outdated now
# Use the following to render your model with grey RGB and no transparency
# show(my_model, (204, 204, 204, 0.0))

# New method to render script results using the CadQuery Gateway Interface
# Use the following to render your model with grey RGB and no transparency
# show_object(result, options={"rgba":(204, 204, 204, 0.0)})

show(res, (204, 204, 204, 0.1))
