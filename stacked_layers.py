# This is a CadQuery script template
# Add your script code below
import cadquery as cq
from Helpers import show
import Helpers

# sample size: 2.5 x 2.5 x 0.025
# considered size for layer sample: 1 x 1 x 0.025
length = 12000
width = 12000
length = 5300
width = 5000

length= 5300
thickness = 250
wall_width = 150

layer_height = 0.866203
radius_rel = 0.38
defect_rel = 0.97
num_cells = 4

# Absolute values
lattice_const = thickness/layer_height
radius = radius_rel*lattice_const
defect= defect_rel*lattice_const

# Alignment Pin parameters
pin_diameter = 2000   #2mm

# Just for truncated structure
width= 2*radius+ 9*lattice_const +50 + 50

layers=[]
for layer_num in xrange(-4,5):

    # Derived Parameters (absolute values)
    n_layers = num_cells*2 +1
    num_rods= n_layers-abs(layer_num)


    outer_point = lattice_const*num_cells*0.5 + (num_cells - abs(layer_num))*0.5*lattice_const


    cut_section_xz= 2*radius+ num_rods*lattice_const +50 +2000

    listOfRods = [(outer_point - j*lattice_const,0) for j in range(0, num_rods ) ]
    grove_position_xy = (width/2.0, length/2*0.8)
    defect_position_xz = (0, -layer_num*thickness)

    res = cq.Workplane("XY").box(width, length, thickness) \
        .faces(">Z").workplane().rect(cut_section_xz,length-2*wall_width).cutThruAll()
    # Cylinders
    bodyWithCylinders= res.faces("<Y").workplane() \
       .pushPoints(listOfRods) \
            .circle(radius).extrude(-length)
    # Cylinders
    bodyWithCylinders= res.faces("<Y").workplane() \
       .pushPoints(listOfRods) \
            .circle(radius).extrude(-length)
    # Core
    withCore= res.faces("<Y").workplane() \
       .pushPoints([defect_position_xz]) \
            .circle(defect).cutThruAll()
    res= res.translate((0,0,250*layer_num))
    res= res.faces(">Z").workplane().\
        pushPoints([(0, -2575), (0, 2575)]).\
            rect(3000, wall_width).cutThruAll()
    layers.append(res)



#### REDO everything
# res2=res.translate((0,0, 500))




####

# Grove for Alignment Pin
# res.faces(">Z").pushPoints([grove_position_xy]).polygon(4, pin_diameter).cutThruAll()


#show_object(res, options={"rgba": (204, 204, 204, 0.0)})
for l in layers:
    show(l, (204, 204, 204, 0.0))
# show(res2, (204, 204, 204, 0.0))

import FreeCADGui as Gui
Gui.SendMsgToActiveView("ViewFit")
Gui.activeDocument().activeView().viewAxonometric()
