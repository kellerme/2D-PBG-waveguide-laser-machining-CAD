# This is a CadQuery script template
# Add your script code below
import cadquery as cq
# import numpy as np
from Helpers import show

# in mm
# center block
height= 15
width= 90
thickness = 10

# holes
diameters= [2.3, 2.2, 2.1, 2.0, 1.9, 1.8, 1.7, 1.6, 1.5, 1.4]
distance = 7
core_depth = 8

# arms
length= width - 10
arm_thickness= 5



result = cq.Workplane("XY").box(thickness, width, height)\

result = result.pushPoints([
                (0, (width-arm_thickness)/2, -(height - arm_thickness)/2 ),
                (0, -(width-arm_thickness)/2, -(height - arm_thickness)/2 )
                ]). \
                box(length, arm_thickness, arm_thickness)

hole_positions= [(0, i*diameters) for i in range(len(diameters))]


result = result.faces(">Z").workplane()
result= result.center(0, -width/2+2*distance)
for d in diameters:
    result= result.circle(d/2)
    result = result.center(0,distance)
    #result= result.circle(diameters[0]/2)
result = result.cutBlind(-core_depth)


# for d in diameters:

# .hole(diameters[0])
# print()

# The show function will still work, but is outdated now
# Use the following to render your model with grey RGB and no transparency
# show(my_model, (204, 204, 204, 0.0))

# New method to render script results using the CadQuery Gateway Interface
# Use the following to render your model with grey RGB and no transparency
# show_object(result, options={"rgba":(204, 204, 204, 0.0)})
show(result)
