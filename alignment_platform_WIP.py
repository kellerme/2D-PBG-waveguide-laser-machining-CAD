# This is a CadQuery script template
# Add your script code below
import cadquery as cq
from Helpers import show
import math

# layer dimensions
layer_x = 20
layer_y = 5.3


# screw diameter: 2.6

# platform
width= 100

#sample platform
width_box= 19
thickness_box= 7
height_box= 6

thickness = 10


# spring
spring_wall_thk = 0.2
spring_height = 5
spring_deg = 25
spring_half_arm= 6


sheet_1 = cq.Workplane("XY").transformed(rotate=(0, 0, spring_deg)).\
    rect(spring_wall_thk,spring_half_arm).\
    extrude(spring_height)
sheet_2 = cq.Workplane("XY").transformed(rotate=(0, 0, -spring_deg)).\
    rect(spring_wall_thk,spring_half_arm).\
    extrude(spring_height)

sheet_3 = sheet_1.mirrorY()

single_cell = sheet_1.union(sheet_2)
#second_cell= single_cell.translate(
#    (spring_half_arm*math.sin(math.radians(spring_deg)) ,0,0))

n_cells = 15
cells = [single_cell.translate(
    (i*spring_half_arm*math.sin(math.radians(spring_deg)) ,0,0))
    for i in range(1,n_cells)]

result= single_cell
for s in cells:
    result=result.union(s)

# Add plates at beginning and end
top_plate=cq.Workplane("XY")\
    .center(-0.5*spring_half_arm*math.sin(math.radians(spring_deg)),0)\
    .rect(spring_wall_thk,
       spring_half_arm*math.cos(math.radians(spring_deg))).extrude(spring_height)
end_plate= top_plate.translate((
    n_cells*spring_half_arm*math.sin(math.radians(spring_deg)),
    0,0))

# New method to render script results using the CadQuery Gateway Interface
# Use the following to render your model with grey RGB and no transparency
# show_object(result, options={"rgba":(204, 204, 204, 0.0)})



# s = cq.Workplane("XY").lineTo()
show(result)
show(top_plate)
show(end_plate)
