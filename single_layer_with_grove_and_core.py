# This is a CadQuery script template
# Add your script code below
import cadquery as cq
from Helpers import show
import Helpers

# sample size: 2.5 x 2.5 x 0.025
# considered size for layer sample: 1 x 1 x 0.025
length = 12000
width = 12000
length = 9000
width = 8000
thickness = 250
wall_width = 200

layer_height = 0.866203
radius_rel = 0.38
defect_rel = 0.97
num_cells = 4
layer_num = -2

# Alignment Pin parameters
pin_diameter = 2000   #2mm

# Derived Parameters (absolute values)
n_layers = num_cells*2 +1
num_rods= n_layers-abs(layer_num)

lattice_const = thickness/layer_height
radius = radius_rel*lattice_const
defect= defect_rel*lattice_const
outer_point = lattice_const*num_cells*0.5 + (num_cells - abs(layer_num))*0.5*lattice_const


cut_section_xz= 2*radius+ num_rods*lattice_const +50

listOfRods = [(outer_point - j*lattice_const,0) for j in range(0, num_rods ) ]
grove_position_xy = (width/2.0, length/2*0.8)
defect_position_xz = (0, -layer_num*thickness)

res = cq.Workplane("XY").box(width, length, thickness) \
    .faces(">Z").workplane().rect(cut_section_xz,length-2*wall_width).cutThruAll()
# Cylinders
bodyWithCylinders= res.faces("<Y").workplane() \
   .pushPoints(listOfRods) \
        .circle(radius).extrude(-length)
# Cylinders
bodyWithCylinders= res.faces("<Y").workplane() \
   .pushPoints(listOfRods) \
        .circle(radius).extrude(-length)
# Core
withCore= res.faces("<Y").workplane() \
   .pushPoints([defect_position_xz]) \
        .circle(defect).cutThruAll()

# Grove for Alignment Pin
res.faces(">Z").pushPoints([grove_position_xy]).polygon(4, pin_diameter).cutThruAll()

# The show function will still work, but is outdated now
# Use the following to render your model with grey RGB and no transparency
# show(my_model, (204, 204, 204, 0.0))

# New method to render script results using the CadQuery Gateway Interface
# Use the following to render your model with grey RGB and no transparency
#show_object(res, options={"rgba": (204, 204, 204, 0.0)})

show(res, (204, 204, 204, 0.0))

import FreeCADGui as Gui
Gui.SendMsgToActiveView("ViewFit")
Gui.activeDocument().activeView().viewAxonometric()
